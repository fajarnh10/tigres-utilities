package com.tigres.model;

import java.io.Serializable;

public class TestingModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4325769054516506689L;

	private String nim;
	private String frontName;
	private String backName;
	private String prodi;
	private String faculty;

	public String getNim() {
		return nim;
	}

	public void setNim(String nim) {
		this.nim = nim;
	}

	public String getFrontName() {
		return frontName;
	}

	public void setFrontName(String frontName) {
		this.frontName = frontName;
	}
	
	public String getBackName() {
		return backName;
	}

	public void setBackName(String backName) {
		this.backName = backName;
	}

	public String getProdi() {
		return prodi;
	}

	public void setProdi(String prodi) {
		this.prodi = prodi;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

}
