package com.tigres.model.test;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class TestingModelResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4325769054516506689L;

	@JsonProperty("nim")
	private String nim;
	
	@JsonProperty("front_name")
	private String frontName;
	
	@JsonProperty("back_name")
	private String backName;
	
	@JsonProperty("prodi")
	private String prodi;
	
	@JsonProperty("faculty")
	private String faculty;
	
	@JsonProperty("status")
	private String status;

	public String getNim() {
		return nim;
	}

	public void setNim(String nim) {
		this.nim = nim;
	}

	public String getFrontName() {
		return frontName;
	}

	public void setFrontName(String frontName) {
		this.frontName = frontName;
	}
	
	public String getBackName() {
		return backName;
	}

	public void setBackName(String backName) {
		this.backName = backName;
	}

	public String getProdi() {
		return prodi;
	}

	public void setProdi(String prodi) {
		this.prodi = prodi;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
