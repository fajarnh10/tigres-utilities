package com.tigres.model.test;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class TestingModelRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4325769054516506689L;

	@JsonProperty("nim")
	private String nim;

	public String getNim() {
		return nim;
	}

	public void setNim(String nim) {
		this.nim = nim;
	}
	
}
