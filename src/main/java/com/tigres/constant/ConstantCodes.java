package com.tigres.constant;

public class ConstantCodes {

	//Integer
	public static final int CONST_INT_CODE_FALSE                    			= 1;
	public static final int CONST_INT_CODE_TRUE         						= 0;
	
	public static final String CONST_STRING_CODE_FALSE                    		= "false";
	public static final String CONST_STRING_CODE_TRUE                    		= "true";
	
	//TESTING API
	public static final String CONST_STRING_CODE_REQUEST_BODY_NULL              = "101";
	public static final String CONST_STRING_CODE_INQUIRY_STUDENT_NIM_NULL       = "111";
	public static final String CONST_STRING_CODE_GENERAL_ERROR              	= "999";
	
}
